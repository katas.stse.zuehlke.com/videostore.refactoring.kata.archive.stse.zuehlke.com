# Video Store Refactoring Kata
This Kata's goal is to rehearse refactoring techniques. It provides an example
which is not properly structured to be extended by new functionalities, but
there are requests from the product owner asking for new functionialites in
order to satisfy new user requirements. Therefore the code must be refactored to
implement the requested functionalities. The participants' task is now to take 
role of the development team and refactor the code such that the requested
functionalities can be implemented in an easy and sustained way.

### Course of this Kata
1. Introduction
    1. Introduction to the topic refactoring
        1. Definition of the term refactoring
        2. Explanation of the refactoring process
        3. Presentation of example refactorings
    2. Presentation of the Kata's task 
2. Solving the Kata
3. Retrospective


### Introduction
#### Introduction to the topic refactoring
##### Definition of the term refactoring

The most famous definition of the term refactoring by Martin Fowler.
> Refactoring is the process of **changing** a software system in such a way
> that it **does not alter the external behavior** of the code yet **improves
> its internal structure**.
> <footer><cite>(M. Fowler)</cite></footer>

Another one stated by my Professor Horst Lichter during my education at the
RWTH Aachen.
> A **change** made to the **internal structure** of software to make it 
> **easier to understand** and **cheaper to modify** **without changing** its
> observable **behavior**.
> <footer><cite>(Prof. H. Lichter, RWTH Aachen)</cite></footer>

##### Explanation of the refactoring process

![Refactoring Process](src/site/resources/images/process/refactoring-process.png)
##### Presentation of example refactorings
Use refactoring card deck and explain how the cards should be read.

#### Task
The example used in this Kata is taken from the introduction section of Martin
Folwer's book *Refactoring: Improving the Design of Existing Code*. The sample
program is vary simple. It is a program to calculate and print a statement of a
customer's charges at a video store. The program is told which movies a customer
rented and for how long. It then calculates the charges, which depend on how
long the movie is rented, and identifies the type movie. There are three kinds
of movies: regular, children's, and new releases. In addition to calculating
charges, the statement also computes frequent renter points, which vary
depending on whether the film is a new release.

##### Domain overview
![Video Store Domain](src/site/resources/images/domain/video-store.png)

##### Overview of the customer class' statement method
![Customer#statement()](src/site/resources/images/interaction/customer/statement.png)

##### Requirements
1.  As a User I would like to get an overview about my rentals made on the video
    store's web site.
2.  As the Manager of the video store I would like to introduce a price
    category for blockbuster.
3.  As the Manager of the video store I would like to change the phrases used in
    the statements.
    
### Solving the Kata
#### Materials
- Beamer
- Laptop connected to beamer
- Card deck with smells and refactorings
- Timout card (green card) for each participant

#### Roles
##### Moderator
The moderator gives a short introduction into the topic and the task to solve.
During the solving phase of the Kata he keeps watch about the rules. After the
Kata is solved he moderates the retrospective.

##### Driver
The driver has control over the keyboard of the laptop and implements the
instructions from the navigator. After the refactoring is done or the
implementation time (5 minutes can be extended by the moderator to 10 minutes)
is over he checks in the code if the test cases are green or he discards the 
code if the test cases are red and the audience does not contradict. After he
has finished he changes role and becomes an observer.

##### Navigator
The navigator selects a refactoring to remove a smell. He guides the driver how
to apply the refactoring. After the refactoring is done or the implementation
time is over the navigator becomes the new driver.

##### Observer
All other partipicitants are observers. They can discuss the steps done be the
implementation team but must not interfere. If there are searious objections
according to refactoring done by the current implementation team, each observer
can raise a green card in order to ask for a timeout. If more than 1/3 of the
participants (also the moderator or the driver can raise a green card) has
raised a green card then the work will be interrupted so that the situation can
be discussed.

In Order to become the next navigator an observers takes a smell and a
refactoring card or defines its own refactoring. When he becomes the new
navigator he explains his selected refactoring (What, Why, How) if it is never
applied before.

#### Style guide
##### Commit message

> ### Header row
> 
> ###### Smell
>   Short description of the removed smell
>
> ###### Refactoring
>   Applied refactoring
>
> ###### Description
>   if necessary
    
### Retrospective
- What do we have done well?
- What do we have learned?
- What would we like to change next time?
- Which parts are still challenging?